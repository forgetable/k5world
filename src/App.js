import React, { Component } from 'react'
import './App.css'

import Header from './Sections/Header'
import Footer from './Sections/Footer'
import Promo from './Sections/Promo'
import Contents from './Sections/Contents'
import { css, StyleSheet } from 'aphrodite/no-important'

import promobg from './img/promobg.jpg'

import videobg from './media/promobg.mp4'

const style = StyleSheet.create({
  main: {
    position: 'fixed',
    backgroundImage: `url(${promobg})`,
    backgroundSize: '100%',
    backgroundPosition: 'center',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    minHeight: '100vh',
    minWidth: '100vw',
    height: '200%',
    zIndex: 1,
    pointerEvents: 'none',
    transform: 'translateY(-25%)',
    filter: 'brightness(1.25)'
  },
  video: {
    minWidth: '100%'
  }
})
class App extends Component {

  render() {
    return (
      <div className="App">
        <div className={css(style.main)}>
          {/* {
            videobg ? (
              <video ref={el => this.video = el} className={css(style.main, style.video)} autoPlay loop>
                <source src={videobg} type="video/mp4" />
                <source src={videobg} type="video/ogg" />
              </video>
            ) : ''
          } */}
        </div>
        
        <div style={{ zIndex: 10, position: 'relative' }}>
          <Header />
          <Promo />
          <Contents />
          <Footer />
        </div>
      </div>
    );
  }
}

export default App
