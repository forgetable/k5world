import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'

import play from '../img/play.png'

const style = StyleSheet.create({
  main: {
    width: '100%',
    maxWidth: 1020,
    margin: '0 auto',
    paddingLeft: 20,
    paddingRight: 20,
    boxSizing: 'border-box',
    height: '100vh',
    minHeight: 500,
    display: 'flex',
    color: '#fff',
    position: 'relative',
    overflow: 'hidden'
  },
  title: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    fontSize: 127,
    zIndex: 15,
    margin: 0,
    alignItems: 'center',
    width: 'auto',
    maxWidth: 1020,
    paddingLeft: 20,
    paddingRight: 20,
    boxSizing: 'border-box',
    display: 'inline-flex',
    '@media(max-width: 980px)': {
      fontSize: 72,
      justifyContent: 'center',
    },
    '@media(max-width: 610px)': {
      fontSize: 50
    },
    '@media(max-width: 374px)': {
      fontSize: 30
    }
  },
  h2: {
    '@media(max-width: 980px)': {
      paddingLeft: '0 !important',
      ':after': {
        display: 'none',
      }
    }
  },
  play: {
    maxHeight: 100,
    marginLeft: 27,
    cursor: 'pointer',
    mixBlendMode: 'light',
    opacity: .75,
    transition: 'opacity .3s ease',
    ':hover': {
      opacity: 1,
    },
    '@media(max-width: 980px)': {
      maxHeight: 75
    },
    '@media(max-width: 610px)': {
      maxHeight: 40,
      marginLeft: 27 / 2
    },
  },
  slogan: {
    position: 'absolute',
    left: 31,
    bottom: '-15%',
    letterSpacing: 1,
    fontSize: 24,
    '@media(max-width: 980px)': {
      fontSize: 18,
      left: 5
    },
    '@media(max-width: 610px)': {
      left: 4,
      bottom: '-25%'
    },
    '@media(max-width: 374px)': {
      top: 43,
      fontSize: 14
    }
  },
  form: {
    position: 'absolute',
    fontSize: 16,
    left: 31,
    top: '110%',
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    '@media(max-width: 980px)': {
      left: 5
    },
    '@media(max-width: 374px)': {
      marginTop: 27
    }
  }
})
class Promo extends Component {
  state = {

  }

  componentDidMount = () => {
    const search = window.location.search.substr(1)
    const params = search.split('&').reduce((obj, next) => {
      const [ key, value ] = next.split('=')
      obj[key] = decodeURI(value)
      return obj
    }, {})
    this.setState(params)
  }

  render() {
    const { name, email, code } = this.state
    return (
      <section className={css(style.main)}>
        <div className={css(style.title)}>
          <h2 style={{ margin: 0, paddingLeft: 20, position: 'relative' }} className={'leftlined ' + css(style.h2)}>k5.world</h2>
          <img className={css(style.play)} src={play} alt="Play video"/>
          <p style={{ paddingLeft: 20 }} className={css(style.slogan)}>Оцифровка цифровой экономики</p>
          
          <div style={{ paddingLeft: 20 }} className={css(style.form)}>
            { code ? (
              <>
                <p style={{ margin: 0 }}>Имя: { name }</p>
                <div style={{ height: 15, background: '#aa1122', width: 2, margin: '0 13.5px' }} />
                <p style={{ margin: 0 }}>E-mail: { email }</p>
                <div style={{ height: 15, background: '#aa1122', width: 2, margin: '0 13.5px' }} />
                <a href={`http://dobro.tech/cgi-bin/dobro.tech/1.0/registration?code=${code}`}>
                  <button style={{ marginTop: 10 }}>Зарегистрироваться</button>
                </a>
              </>
            ) : (
              <p style={{ fontSize: 14 }}>Вам нужно приглашение, чтобы зарегистрироваться</p>
            )}
            
          </div>
          
        </div>
      </section>
    )
  }
}

export default Promo