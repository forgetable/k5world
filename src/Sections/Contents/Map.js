import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'

import map from '../../img/k5worldmap.png'

const style = StyleSheet.create({
  main: {
    position: 'relative',
    maxWidth: 1020,
    paddingLeft: 20,
    paddingRight: 20,
    boxSizing: 'border-box',
    width: '100%',
    margin: '100px auto 0 auto',
    paddingBottom: 100,
    zIndex: 2
  },
  map: {
    width: '100%',
    height: '45vw',
    backgroundImage: `url(${map})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    '@media(max-width: 800px)': {
      backgroundSize: '150%'
    },
    '@media(max-width: 450px)': {
      backgroundSize: '220%'
    }
  }
})


class Map extends Component {
  state = {

  }

  render() {

    return (
      <div className={css(style.main)}>
        <div className={css(style.map)} />
      </div>
    )
  }
}

export default Map