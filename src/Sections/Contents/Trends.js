import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'

const style = StyleSheet.create({
  main: {
    position: 'relative',
    maxWidth: 1020,
    paddingLeft: 20,
    paddingRight: 20,
    boxSizing: 'border-box',
    width: '100%',
    margin: '100px auto',
    zIndex: 2
  },
  panel: {
    background: '#fff',
    minHeight: 700,
    marginTop: 54,
    display: 'flex',
    flexWrap: 'wrap',
    padding: 27 * 4,
    justifyContent: 'space-around',
    alignItems: 'center',
    borderRadius: 1,
    '@media(max-width: 800px)': {
      padding: 20,
      paddingTop: 27 * 3
    }
  },
  trend: {
    marginBottom: 54
  },
  panelHalf: {
    height: '100%',
    display: 'flex',
    maxWidth: 325,
    width: '100%',
    flexDirection: 'column',
    color: '#020088'
  },
  trendHeading: {
    fontSize: 25,
    textTransform: 'uppercase',
    margin: 0,
    marginBottom: 10,
    '@media(max-width: 500px)': {
      fontSize: 20
    }
  }
})

const Trend = ({ heading, text }) => (
  <div className={css(style.trend)}>
    <h3 className={css(style.trendHeading)}>{ heading }</h3>
    <p className={css(style.trendText)}>{ text }</p>
  </div>
)

class Trends extends Component {
  state = {

  }

  render() {

    return (
      <div className={css(style.main)}>
        <h1 className="leftlined llblue" style={{ position: 'relative', paddingLeft: 30, fontSize: 60 }}>Тренды</h1>
        <div className={css(style.panel)}>
          <div className={css(style.panelHalf)}>
            <Trend heading="Цифровая экономика" text="Обработка, хранение и передача информации программно-техническими средствами" />
            <Trend heading="Образование" text="Каким будет рынок труда в 2030 году, а в 2050-м? Мы сегодня не знаем, чему учить наших детей! То, что они сейчас изучают в школе, к их сорокалетию утратит актуальность" />
            <Trend heading="Немного предприниматели" text='В ближайшие 10 лет более 1 млрд людей станут "немного-предпринимателями"' />
            <Trend heading="Деньги" text="Деньги перестанут выполнять роль всеобщего эквивалента и меры стоимости" />
            <Trend heading="Экономика" text="Новая космическая экономика" />            
          </div>
          <div className={css(style.panelHalf)}>
            <Trend heading="Оцифровка" text="Новый продукт — оцифровка данных, идей, проектов" />
            <Trend heading="Навыки будущего" text="Создавать такие специальности, в которых люди будут востребованы в эпоху искусственного интеллекта и автоматической работы алгоритмов" />
            <Trend heading="Нано-инвесторы" text='В течение 5 лет более 3 млрд людей станут "Нано-инвесторами"' />
            <Trend heading="Капитал" text="Вместо нескольких процентов в год, Цифровой капитал сможет прирастать с огромной скоростью: ежедневно, ежечасно, ежеминутно." />
            <Trend heading="Торговля" text="Распознавание речи, новый вид торговли" />
          </div>
        </div>
      </div>
    )
  }
}

export default Trends