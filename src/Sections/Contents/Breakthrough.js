import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'

import putinPic from '../../img/putin.jpg'

const style = StyleSheet.create({
  main: {
    position: 'relative',
    width: '100%',
    maxWidth: 1020,
    paddingLeft: 20,
    paddingRight: 20,
    boxSizing: 'border-box',
    margin: '150px auto',
    color: '#fff'
  },
  h1: {
    position: 'relative', 
    paddingLeft: 30, 
    color: '#fff', 
    fontSize: 60
  },
  top: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 54,
  },
  topText: {
    fontSize: 40,
    fontWeight: 700,
  },
  topImg: {
    borderRadius: 2,
    maxWidth: '100%'
  },
  bottom: {
    fontSize: 20
  }
})

class Breakthrough extends Component {
  state = {

  }

  render = () => {
    return (
      <div className={css(style.main)}>
        <h1 className={'leftlined ' + css(style.h1)}>Прорыв</h1>
        <div className={css(style.top)}>
          <img className={css(style.topImg)} src={putinPic} alt="Putin"/>
          <p className={css(style.topText)}>k5.world - команда Путина</p>
        </div>
        <div className={css(style.bottom)}>
          <p style={{ marginBottom: 0 }}>"Главное - благополучие человека"</p>
          <p style={{ marginTop: -2, fontSize: 14, fontStyle: 'italic' }}>Послание президента и майский указ 2018г.</p>
        </div>
      </div>
    )
  }
}

export default Breakthrough