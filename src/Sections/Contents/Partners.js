import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'

import kremlinpic from '../../img/kremlin.jpg'
import rubikpic from '../../img/rubik.jpg'
import k5logo from '../../img/k5logo.jpg'


const style = StyleSheet.create({
  main: {
    width: '100%',
    maxWidth: 1020,
    paddingLeft: 20,
    paddingRight: 20,
    boxSizing: 'border-box',
    color: '#fff',
    margin: '400px auto 0 auto',
    zIndex: 1,
    '@media(max-width: 485px)': {
      marginTop: 200
    },
    '@media(max-width: 360px)': {
      marginTop: 140
    }
  },
  h1: {
    fontSize: 60,
    position: 'relative',
    paddingLeft: 30,
    '@media(max-width: 450px)': {
      fontSize: 40
    }
  },
  partnerCard: {
    display: 'flex',
    margin: '50px 0',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  cardPhoto: {
    width: 400,
    height: 500,
    backgroundColor: '#eee',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    '@media(max-width: 690px)': {
      height: 300
    },
    '@media(max-width: 450px)': {
      height: 200
    }
  },
  cardDesc: {
    width: 400,
    display: 'flex',
    flexDirection: 'column',
    '@media(max-width: 870px)': {
      padding: 10
    },
    '@media(max-width: 690px)': {
      fontSize: 14
    }
  },
  cardHeading: {
    fontSize: 40,
    color: '#E83535',
    textShadow: '0px 0px 1px rgba(0,0,0,.4)',
    '@media(max-width: 870px)': {
      fontSize: 30
    },
    '@media(max-width: 690px)': {
      fontSize: 25
    }
  },
  cardLink: {
    marginBottom: 50,
    color: '#fff'
  },
  cardQuoteOwner: {
    textAlign: 'right',
    fontSize: 14,
    color: '#eee',
    maxWidth: 250,
    alignSelf: 'flex-end',
    marginTop: 0
  }
})

const Card = props => {
  const { photoPosition = 'left', src, heading, link, desc, quoteOwner } = props

  const imgPart = <div className={css(style.cardPhoto)} style={{ backgroundImage: `url(${src})` }} />
  const textPart = (
    <div className={css(style.cardDesc)}>
      <h2 className={css(style.cardHeading)}>
        { heading }
      </h2>
      <p><a className={css(style.cardLink)} href={link} target="_blank" rel="noreferrer noopener">{link}</a></p>
      <p>{ desc }</p>
      {
        quoteOwner ? <p className={css(style.cardQuoteOwner)}>{ quoteOwner }</p> : ''
      }
    </div>
  )

  return (
    <div className={css(style.partnerCard)}>
      {
        photoPosition === 'left' ? imgPart : textPart
      }
      {
        photoPosition === 'left' ? textPart : imgPart
      }
    </div>
  )
}

class Partners extends Component {
  state = {
    
  }

  render() {

    return (
      <div className={css(style.main)}>
        <h1 className={css(style.h1) + ' leftlined'}>Партнёры</h1>
        <Card 
          src={kremlinpic} 
          heading={'АНО "Институт развития цифровой экономики"'} 
          link="http://deei.ru" 
          desc={'"...и теперь о Цифровизации: необходимо и мы будем поддерживать институты развития..."'}
          quoteOwner="В.В. Путин (27.12.2018г. на встрече с предпринимательским сообществом)"
        />
        <Card 
          photoPosition="right"
          src={rubikpic} 
          heading={'Фонд содействия развитию высоких технологий "Добротех"'} 
          link="http://dobro.tech" 
          desc="Платформа Dobro.tech - оцифровка Данных, Идей и Проектов. Обучение и Тренажёры"
        />
        <Card 
          src={k5logo} 
          heading="Корпорация свободного общения" 
          link="http://k5.ru" 
          desc="КСО — уникальные прорывные технологии, у которых нет аналогов в мире"
        />
      </div>
    )
  }
}

export default Partners