import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'

const style = StyleSheet.create({
  main: {
    position: 'relative',
    width: '100%',
    maxWidth: 1020,
    paddingLeft: 20,
    paddingRight: 20,
    boxSizing: 'border-box',
    margin: '150px auto',
    color: '#fff'
  },
  h1: {
    position: 'relative',
    paddingLeft: 30,
    color: '#fff',
    fontSize: 60,
    '@media(max-width: 450px)': {
      fontSize: 40
    }
  },
  top: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 27 / 2,
    flexWrap: 'wrap'
  },
  topPart: {
    maxWidth: 400
  },
  topPartHeading: {
    fontSize: 20,
    marginBottom: 27
  },
  cta: {
    marginTop: 27 * 2
  },
  form: {
    fontSize: 16,
    display: 'inline-flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 27 / 2,
    paddingLeft: 0,
    position: 'relative',
    left: '50%',
    transform: 'translatex(-50%)',
    flexWrap: 'wrap',
    '@media(max-width: 374px)': {
      marginTop: 15
    }
  },
  faqLink: {
    color: '#fff',
  },
  faq: {
    textAlign: 'center',
    marginTop: 27 * 2
  }
})

class Motivation extends Component {
  state = {

  }

  componentDidMount = () => {
    const search = window.location.search.substr(1)
    const params = search.split('&').reduce((obj, next) => {
      const [key, value] = next.split('=')
      obj[key] = decodeURI(value)
      return obj
    }, {})
    this.setState(params)
  }

  render = () => {
    const { name, email, code } = this.state

    return (
      <div className={css(style.main)}>
        <h1 className={'leftlined ' + css(style.h1)}>Мотивация</h1>
        <div className={css(style.top)}>
          <div className={css(style.topPart)}>
            <p className={css(style.topPartHeading)}>Сегодня развивается рынок технологий</p>
            <p>
              Вы можете инвестировать время и деньги в технологии и конкурировать с мировыми гигантами
            </p>
          </div>
          <div className={css(style.topPart)}>
            <p className={css(style.topPartHeading)}>Завтра будет рынок идей, творчества, креатива</p>
            <p>
              Вы можете стать первыми в оцифровке:<br />Без вложений, затрат времени и специальных знаний
            </p>
          </div>
        </div>
        <div className={css(style.cta)}>
          <h1 style={{ textAlign: 'center' }}>Выбор очевиден</h1>
        </div>
        <div style={{ paddingLeft: 20 }} className={css(style.form)}>
          { code ? (
            <>
              <p style={{ margin: 0 }}>Имя: { name }</p>
              <div style={{ height: 15, background: '#aa1122', width: 2, margin: '0 13.5px' }} />
              <p style={{ margin: 0 }}>E-mail: { email }</p>
              <div style={{ height: 15, background: '#aa1122', width: 2, margin: '0 13.5px' }} />
              <a href={`http://dobro.tech/cgi-bin/dobro.tech/1.0/registration?code=${code}`}>
                <button style={{ marginTop: 10 }}>Зарегистрироваться</button>
              </a>
            </>
          ) : (
            <p style={{ fontSize: 14 }}>Вам нужно приглашение, чтобы зарегистрироваться</p>
          )}
        </div>
        <div className={css(style.faq)}>
          <a className={css(style.faqLink)} href="https://docs.google.com/document/d/1uL-7uyJ5QxshBUhDGSC4CYfOaKa9SEST86t5oAEpjls/edit?usp=sharing" target="_blank" rel="noopener noreferrer">F.A.Q - если остались вопросы</a>
        </div>
      </div>
    )
  }
}

export default Motivation