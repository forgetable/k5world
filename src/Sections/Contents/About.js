import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'

const style = StyleSheet.create({
  main: {
    zIndex: 10,
    width: '100%',
    minHeight: '100vh',
    boxSizing: 'border-box',

  },
  paragraphs: {
    display: 'flex',
    justifyContent: 'space-between',
    marginTop: 27,
    padding: '10px 30px',
    '@media(max-width: 975px)': {
      padding: '10px 0'
    },
    '@media(max-width: 440px)': {
      paddingBottom: 150
    }
  },
  p: {
    width: '100%',
    maxWidth: '400px',
    fontSize: 14,
    '@media(max-width: 975px)': {
      padding: '0 10px'
    }
  },
  h2_1: {
    fontSize: 60, 
    marginTop: 27, 
    color: '#E83535',
    '@media(max-width: 880px)': {
      fontSize: 40
    }
  },
  h2_2: {
    fontSize: 45, 
    color: '#E83535',
    '@media(max-width: 880px)': {
      fontSize: 33
    }
  }
})

class About extends Component {
  state = {

  }

  render() {
    const { vrimg: vr, vrclassName } = this.props
    return (
      <div className={css(style.main)}>
        <div style={{ padding: '0 30px', position: 'relative' }}>
          <h3 style={{ textTransform: 'uppercase', color: '#3552E8' }}>О проекте</h3>
          <h2 className={css(style.h2_1)}>k5.world — </h2>
          <h2 className={'leftlined llred ' + css(style.h2_2)}>Оцифровка, приносящая прибыль</h2>
        </div>
        <div className={css(style.paragraphs)}>
          <p className={css(style.p)}>
            Сегодня люди оцифрованы государством, банками, социальными сетями, интернет магазинами и т.д. При этом оцифрованные персональные данные позволяют получать доходы вышеуказанным структурам, а не человеку. Но личные данные изначально принадлежать человеку, поэтому "Оцифровка" должна быть в интересах человека и приносить ему прибыль.
          </p>
          <p className={css(style.p)}>
            Насколько качественно и ответственно человек отнесётся к выполняемой задаче, настолько будет и результат. Для этого необходимо получить базовые знания по "Цифровой грамоте" и выполнять "Цифровую гигиену", т.к. "Сеть полна лжецов" и доступ к Оцифрованным личным (персональным) данным может нанести вред личности (человеку).
          </p>
        </div>
        <img className={vrclassName} src={vr} alt="New Tech" />
      </div>
    )
  }
}

export default About