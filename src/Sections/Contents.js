import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'
import { Parallax, Background } from 'react-parallax'

import aboutbg from '../img/aboutbg.jpg'
import mainbg from '../img/mainbg1.jpg'
import vr from '../img/vr.jpg'

import building from '../img/building.png'

import About from './Contents/About'
import Partners from './Contents/Partners'
import Trends from './Contents/Trends'
import Breakthrough from './Contents/Breakthrough'
import Motivation from './Contents/Motivation'
import Map from './Contents/Map'

const addr = 'http://k5.ru:3000?name="Иван"'

const style = StyleSheet.create({
  main: {
    position: 'relative'
  },
  vrtop: {
    position: 'absolute',
    left: '50%',
    width: '100%',
    maxWidth: 1020,
    paddingLeft: 20,
    paddingRight: 20,
    boxSizing: 'border-box',
    bottom: 0,
    transform: 'translate(-50%, 50%)'
  },
  vrbottom: {
    position: 'absolute',
    left: '50%',
    width: '100%',
    maxWidth: 1020,
    paddingLeft: 20,
    paddingRight: 20,
    boxSizing: 'border-box',
    top: 0,
    transform: 'translate(-50%, -50%)'
  },
  mainbg: {
    filter: 'brightness(.8)'
  },
  about: {
    margin: '0 auto 0 auto',
    padding: '81px 20px 0 20px',
    maxWidth: 1020,
    zIndex: 12
  }
})

class Contents extends Component {
  state = {

  }

  render() {

    return (
      <div className={css(style.main)}>
        <Parallax
            // blur={{ min: -1, max: 3 }}
            bgImage={aboutbg}
            bgImageAlt="the cat"
            strength={0}
            renderLayer={percentage => (
              <div style={{ position: 'relative', overflow: 'visible' }}>
                <div style={{ position: 'relative', overflow: 'hidden' }}>
                  <div style={{
                    position: "absolute",
                    background: `url(${aboutbg})`,
                    left: 0,
                    top: 0,
                    bottom: 0,
                    right: 0,
                    zIndex: 10,
                    backgroundSize: 'cover',
                    transform: `translateY(${percentage * 6000 - 3000}px)`,
                    // opacity: percentage
                  }} />
                  <div className={css(style.about)} style={{ position: 'relative', minHeight: '100vh', zIndex: 11 }}>
                    <About vrimg={vr} vrclassName={css(style.vrtop)} />
                  </div>
                </div>

                <div style={{
                  position: 'relative', overflow: 'visible', backgroundSize: 'cover',
                  backgroundImage: 'linear-gradient(165deg, #112575 0%, #2BC0DB 50%, #E837B3, #F02C54)', }}>
              
                <div style={{ display: 'flex', zIndex: 1 }}>
                  <img className={css(style.vrbottom)} src={vr} alt="New Tech" />
                  <Partners />
                </div>
                <div style={{ minHeight: '100vh', width: '91%', transform: 'translateX(-50%)', left: '50%', position: 'relative', overflow: 'hidden'  }}>
                  <div style={{
                    position: "absolute",
                    background: `url(${building})`,
                    left: 0,
                    top: 0,
                    bottom: 0,
                    right: 0,
                    height: '100%',
                    backgroundSize: 'cover',
                    transform: `translateY(${(percentage * 6000) - 4400}px) scale(1)`,
                    backgroundPosition: 'center',
                    // filter: 'blur(2px)'
                  }}/>
                  <Trends />
                </div>
                <Breakthrough />
                <Motivation />
                <Map />
              </div>
            </div>
          )}
        />
      </div>
    )
  }
}

export default Contents