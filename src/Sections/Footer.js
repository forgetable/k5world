import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'

const style = StyleSheet.create({
  main: {
    textAlign: 'center',
    color: '#fff',
    backgroundImage: 'linear-gradient(82.5deg, #02005F, transparent, #2BC0DB)',
    position: 'relative',
    height: 90,
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    fontSize: 20,
  }
})

class Footer extends Component {
  state = {
    
  }

  render() {

    return (
      <footer className={css(style.main)}>
        © 2019 k5.world | Мир, который строим вместе
      </footer>
    )
  }
}

export default Footer