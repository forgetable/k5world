import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'

const style = StyleSheet.create({
  main: {
    position: 'fixed',
    top: 27 / 2,
    width: '90%',
    left: '50%',
    transform: 'translateX(-50%)',
    height: 72,
    color: '#fff',
    margin: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    zIndex: 1000,
    mixBlendMode: 'difference'
  },
  list: {
    listStyle: 'none',
    // display: 'flex',
    display: 'none',
    '@media(max-width: 800px)': {
      fontSize: 14
    },
    '@media(max-width: 678px)': {
      fontSize: 10
    }
  },
  listItem: {
    marginLeft: 13.5,
    cursor: 'pointer',
    whiteSpace: 'nowrap',
    transition: 'color .3s ease',
    ':first-child': {
      marginLeft: 0
    },
    ':hover': {
      color: 'red'
    }
  },
  heading: {
    textTransform: 'uppercase',
    '@media(max-width: 800px)': {
      fontSize: 25
    },
    '@media(max-width: 678px)': {
      fontSize: 20
    }
  }
})

class Header extends Component {
  state = {

  }

  render() {
    
    return (
      <header className={css(style.main)}>
        <h1 className={css(style.heading)}>Мир, который строим вместе</h1>
        <nav>
          <ul className={css(style.list)}>
            <li className={css(style.listItem)}>
              О проекте
            </li>
            <li className={css(style.listItem)}>
              Партнёры
            </li>
            <li className={css(style.listItem)}>
              Тренды
            </li>
            <li className={css(style.listItem)}>
              Прорыв
            </li>
            <li className={css(style.listItem)}>
              Мотивация
            </li>
          </ul>
        </nav>
      </header>
    ) 
  }
}

export default Header